package simpledb;

public interface EvictionStrategy {

	public void use(int uid);
	public void remove(int uid);
	public int evict();
}
