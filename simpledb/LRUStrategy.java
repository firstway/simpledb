package simpledb;

import java.util.LinkedList;
import java.util.List;

public class LRUStrategy implements EvictionStrategy {

	List<Integer> cached = new LinkedList<Integer>();
	
	@Override
	public void use(int uid) {
		this.cached.remove(new Integer(uid));
		this.cached.add(new Integer(uid));
	}

	@Override
	public int evict() {
		if(this.cached.size()>0){
			//return the first one
			return this.cached.remove(0);
		}
		return -1;
	}
	
	@Override
	public void remove(int uid) {
		this.cached.remove(new Integer(uid));
	}

}
